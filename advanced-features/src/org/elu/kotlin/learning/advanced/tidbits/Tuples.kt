package org.elu.kotlin.learning.advanced.tidbits

fun capitalAndPopulation(country: String): Pair<String, Long> {
    return Pair("Madrid", 2300000)
}

fun countryInformation(country: String): Triple<String, String, Long> {
    return Triple("Madrid", "Europe", 2300000)
}

data class CustomerKotlin(var id: Int, var name: String, var email: String)

fun main(args: Array<String>) {
    val result = capitalAndPopulation("Spain")
    println(result.first)
    println(result.second)

    val countryInfo = countryInformation("Spain")
    println(countryInfo.first)
    println(countryInfo.second)
    println(countryInfo.third)

    val (capital, population) = capitalAndPopulation("Spain")
    println(capital)
    println(population)

    val (id, name, email) = CustomerKotlin(1, "Edu", "email")
    println(id)
    println(name)
    println(email)

    val listCaptialsAndCountries = listOf(Pair("Madrid", "Spain"), "Paris" to "France")
    for ((capital2, country) in listCaptialsAndCountries) {
        println("$capital2 - $country")
    }
}
