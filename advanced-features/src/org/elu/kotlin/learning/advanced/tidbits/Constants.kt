package org.elu.kotlin.learning.advanced.tidbits

object Copyright {
    const val author = "Edu"
}

const val CopyrightAuthor = "Edu"

fun main(args: Array<String>) {
    println(Copyright.author)
    println(CopyrightAuthor)
}
