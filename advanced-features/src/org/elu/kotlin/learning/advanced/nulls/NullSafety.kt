package org.elu.kotlin.learning.advanced.nulls

class Service {
    fun evaluate() {}
}

class ServiceProvider {
    fun createServices(): Service? {
        return Service()
    }
}

fun main(args: Array<String>) {
    val message = "Message"
    println(message.length)

    var nullMessage: String? = null
    if (nullMessage != null) {
        println(nullMessage.length)
    }
    println(nullMessage?.length)

    nullMessage = "Hello"
    println(nullMessage.length)

    val sp = createServiceProvider()
    sp?.createServices()?.evaluate()
}

private fun createServiceProvider(): ServiceProvider? = ServiceProvider()
