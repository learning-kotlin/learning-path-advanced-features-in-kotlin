package org.elu.kotlin.learning.advanced.inheritance

open class Person {
    open fun validate() {}
}

open class Customer: Person {
    final override fun validate() {}

    constructor(): super() {}
}

class SpecialCustomer: Customer() {
    // cannot override validate function
}

data class CustomerEntity(val name: String): Person()

fun main(args: Array<String>) {
    val customer = Customer()
    customer.validate()
}
