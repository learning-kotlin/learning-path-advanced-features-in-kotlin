package org.elu.kotlin.learning.advanced.inheritance

interface CustomerRepository {
    val isEmpty: Boolean
        get() = true
    fun store(obj: Customer) {
        // some implementation
    }
    fun getById(id: Int): Customer
}

class SQLCustomerRepository: CustomerRepository {
    override fun getById(id: Int): Customer {
        return Customer()
    }

    override val isEmpty: Boolean
        get() = false

    override fun store(obj: Customer) {
        // own implementation
    }
}

interface Interface1 {
    fun funA() {
        println("Fun A from Interface 1")
    }
}

interface Interface2 {
    fun funA() {
        println("Fun A from Interface 2")
    }
}

class Class1And2: Interface1, Interface2 {
    override fun funA() {
        println("Our own")
        super<Interface2>.funA()
    }
}

fun main(args: Array<String>) {
    val c = Class1And2()
    c.funA()
}
